//
//  CategoryTableViewController.swift
//  ToDoey
//
//  Created by Gerson Costa on 20/11/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class CategoryTableViewController: SwipeTableViewController {

    var categories: Results<Category>?
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()
    }
    
    // MARK: - TableView Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ToDoListViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categories?[indexPath.row]
        }
    }
    
    // MARK: - TableView DataSource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        guard let category = categories?[indexPath.row] else { fatalError() }
        
        guard let categoryColor = UIColor(hexString: category.cellColor) else { fatalError() }
        
        cell.backgroundColor = categoryColor
        cell.textLabel?.textColor = ContrastColorOf(categoryColor, returnFlat: true)
        
        cell.textLabel?.text = categories?[indexPath.row].name ?? "No categories?_? Add a new category!!!"
        
        return cell
        
    }
    
    // MARK: - Delete Data From Swipe
    
    override func updateModel(at indexPath: IndexPath) {
        if let category = categories?[indexPath.row] {
            do {
                try realm.write {
                    realm.delete(category)
                }
            } catch {
                print("Error trying to delete category. Error: \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Data Manipulation Methods
    
    func save(category: Category) {
        
        do {
            try realm.write {
                realm.add(category)
            }
        } catch {
            print("Error saving data. Error: \(error.localizedDescription)")
        }
        tableView.reloadData()
    }
    
    func loadCategories() {
        
         categories = realm.objects(Category.self)
        
        tableView.reloadData()
    }
    
    @IBAction func addBtnPressed(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Add new item", message: "", preferredStyle: .alert)
        var textfield = UITextField()
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Add item", style: .default, handler: { (action) in
            if textfield.text != "" {
                print("Successfully added \(textfield.text!)!")
                
                let newCategory = Category()
                newCategory.name = textfield.text!
                newCategory.cellColor = UIColor.randomFlat.hexValue()
                
                self.save(category: newCategory)
            }
        }))
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Add new category"
            textfield = alertTextField
        }
        
        present(alert, animated: true, completion: nil)
        
    }
    
}
